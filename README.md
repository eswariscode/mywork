
## How to build
 mvn clean package -Dmaven.test.skip=true

---

## Run spring boot jar

java -jar target/bookstore-0.0.1-SNAPSHOT.jar

---

## List of APIs
1. GET /user/ping            HealthCheck
2. GET /book                 GetAllBooks, does not require authentication
3. POST /auth/login          Login
4. GET /user                 Get logged in user info, this works post login. Throws 403 if user is not loggedin
5. POST /user/register       Register New user
6. GET /order                Get all orders of a user. Authentication required
7. POST /order               Place an book order
8. GET /auth/logout          logout

---

## Here you go for testing

Navigate to path mywork/src/test/resources/request and issue the following CURL command:

1. TestAPI:            curl -i -X GET http://localhost:8080/user/ping
2. GetALLBooks API :   curl -i -H "Accept: application/json" -X GET http://localhost:8080/book

## CURL to Test Already Regsitered user:
-------------------------------
1. curl -i -H "Accept: application/json" -H "Content-type: application/json" -X POST http://localhost:8080/auth/login  -d @loginReq.json -c cookies.txt
2. curl -i -H "Accept: application/json" -X GET http://localhost:8080/user -b cookies.txt
3. curl -i -H "Accept: application/json" -X GET http://localhost:8080/order -b cookies.txt
4. curl -i -H "Accept: application/json" -H "Content-type: application/json" -X POST http://localhost:8080/order -d @orderReq.json -b cookies.txt
5. curl -i -X GET http://localhost:8080/auth/logout

## Test New Registration:
-------------------------------
1. curl -i -H "Accept: application/json" -H "Content-type: application/json" -X POST http://localhost:8080/user/register  -d @registerReq.json
2. curl -i -H "Accept: application/json" -H "Content-type: application/json" -X POST http://localhost:8080/auth/login  -d @newLoginReq.json -c cookies.txt
3. curl -i -H "Accept: application/json" -X GET http://localhost:8080/user -b cookies.txt
4. curl -i -X GET http://localhost:8080/auth/logout

